package main

import (
	"context"

	"github.com/gin-gonic/gin"
	"gitlab.com/wojtii/dc-challenge/internal/api"
	"gitlab.com/wojtii/dc-challenge/internal/payment"
	"gitlab.com/wojtii/dc-challenge/internal/postgres"
	"go.uber.org/zap"
)

func main() {
	logger := zap.NewExample()
	defer func() { _ = logger.Sync() }()

	undo := zap.ReplaceGlobals(logger)
	defer undo()

	g := gin.New()

	ctx := context.Background()
	db, err := postgres.Setup(ctx)
	must(err)

	repository := payment.NewPostgresRepository(db)

	err = repository.Migrate(ctx)
	must(err)

	api.Init(g, repository)

	err = g.Run()
	must(err)
}

func must(err error) {
	if err != nil {
		zap.S().Errorw("error when running api", zap.Error(err))
		panic(err)
	}
}
