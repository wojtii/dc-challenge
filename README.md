# Tech stack

- Go 1.17
- Postgres 14
- Docker (for integration tests)

# Testing

## Unit tests

1. `make test`

## Integration tests

1. `touch .env`
1. Example content of `.env` file

```
POSTGRES_USER=postgres
POSTGRES_DB=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_HOST=database
```

1. `docker-compose up -d --build`
1. `make integration-test`
1. `docker-compose down`

# Improvements / Ideas

- Client authentication is 100% needed

- Better error handling for validation errors - now it just returns

```json
{ "error": "validation error" }
```

- Validation for provided amount - do not allow or trim more than two digits after a decimal

- Refactor integration tests - I did not have a lot of time to write them so they are not clean

- Write separate tests for database layer, to test edge cases and concurrent modifications

- Write tests for validations

- Use proper migration tool instead of single sql, like https://github.com/golang-migrate/migrate

- During capture or refund check if card is not expired, because user may add a valid card, wait after the card will expire and then try to capture or refund

- Should credit card cvv be stored?

- Support more currencies

- Support capture and refund between different currencies

- In production postgres config (especially password) should be loaded from vault not from environment variables, but it is fine for local development

- Add error wrapping everywhere like `fmt.Errorf("failed to authorize: %w", err)`, for easier debugging,
