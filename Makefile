test:
	TZ=UTC go test ./... -v -count=1 -cover -short

integration-test:
	TZ=UTC go test ./... -count=1
