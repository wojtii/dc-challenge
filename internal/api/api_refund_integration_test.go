package api_test

import (
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestIntegration_Refund(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	t.Parallel()

	c := http.Client{Timeout: time.Second * 10}
	authorizeReqBody := `{
		"creditCard": {
			"number": "2222410740360010",
			"cvv": "737",
			"expiresAt": {
				"year": 2050,
				"month": 12
			}
		},
		"amount": {
			"amount": "10000",
			"currency": "PLN"
		}
	}`
	authorizeResBody, res, err := authorizeRequest(c, authorizeReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusCreated, res.StatusCode)

	payment, ok := authorizeResBody["payment"].(map[string]interface{})
	require.True(t, ok)

	id, ok := payment["id"].(string)
	require.True(t, ok)
	require.NotEmpty(t, id)
	require.Equal(t, map[string]interface{}{"amountAvailable": "10000", "currency": "PLN"}, payment["amount"])

	captureReqBody := `{
		"amount": "999.99"
	}`
	captureResBody, res, err := captureRequest(c, id, captureReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, res.StatusCode)

	payment, ok = captureResBody["payment"].(map[string]interface{})
	require.True(t, ok)
	require.Equal(t, map[string]interface{}{"amountAvailable": "9000.01", "currency": "PLN"}, payment["amount"])

	refundReqBody := `{
		"amount": "9000.02"
	}`
	refundResBody, res, err := refundRequest(c, id, refundReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusBadRequest, res.StatusCode)
	require.Equal(t, map[string]interface{}{"error": "amount is too big"}, refundResBody)

	refundReqBody = `{
		"amount": "9000"
	}`
	refundResBody, res, err = refundRequest(c, id, refundReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, res.StatusCode)

	payment, ok = refundResBody["payment"].(map[string]interface{})
	require.True(t, ok)
	require.Equal(t, map[string]interface{}{"amountAvailable": "0.01", "currency": "PLN"}, payment["amount"])

	refundReqBody = `{
		"amount": "0.01"
	}`
	refundResBody, res, err = refundRequest(c, id, refundReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, res.StatusCode)

	payment, ok = refundResBody["payment"].(map[string]interface{})
	require.True(t, ok)
	require.Equal(t, map[string]interface{}{"amountAvailable": "0", "currency": "PLN"}, payment["amount"])

	refundReqBody = `{
		"amount": "0.01"
	}`
	refundResBody, res, err = refundRequest(c, id, refundReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusBadRequest, res.StatusCode)
	require.Equal(t, map[string]interface{}{"error": "payment is not eligible for this action"}, refundResBody)
}

func refundRequest(c http.Client, id, body string) (map[string]interface{}, *http.Response, error) {
	return request(c, http.MethodPut, baseUrl+"/"+id+"/refund", body)
}
