package api_test

import (
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestIntegration_Capture(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	t.Parallel()

	c := http.Client{Timeout: time.Second * 10}
	authorizeReqBody := `{
		"creditCard": {
			"number": "2222410740360010",
			"cvv": "737",
			"expiresAt": {
				"year": 2030,
				"month": 3
			}
		},
		"amount": {
			"amount": "123.45",
			"currency": "EUR"
		}
	}`
	authorizeResBody, res, err := authorizeRequest(c, authorizeReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusCreated, res.StatusCode)

	payment, ok := authorizeResBody["payment"].(map[string]interface{})
	require.True(t, ok)

	id, ok := payment["id"].(string)
	require.True(t, ok)
	require.NotEmpty(t, id)
	require.Equal(t, map[string]interface{}{"amountAvailable": "123.45", "currency": "EUR"}, payment["amount"])

	captureReqBody := `{
		"amount": "1000.32"
	}`
	captureResBody, res, err := captureRequest(c, id, captureReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusBadRequest, res.StatusCode)
	require.Equal(t, map[string]interface{}{"error": "amount is too big"}, captureResBody)

	captureReqBody = `{
		"amount": "100.32"
	}`
	captureResBody, res, err = captureRequest(c, id, captureReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, res.StatusCode)

	payment, ok = captureResBody["payment"].(map[string]interface{})
	require.True(t, ok)
	require.Equal(t, map[string]interface{}{"amountAvailable": "23.13", "currency": "EUR"}, payment["amount"])

	captureReqBody = `{
		"amount": "10.13"
	}`
	captureResBody, res, err = captureRequest(c, id, captureReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, res.StatusCode)

	payment, ok = captureResBody["payment"].(map[string]interface{})
	require.True(t, ok)
	require.Equal(t, map[string]interface{}{"amountAvailable": "13", "currency": "EUR"}, payment["amount"])

	captureReqBody = `{
		"amount": "13"
	}`
	captureResBody, res, err = captureRequest(c, id, captureReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, res.StatusCode)

	payment, ok = captureResBody["payment"].(map[string]interface{})
	require.True(t, ok)
	require.Equal(t, map[string]interface{}{"amountAvailable": "0", "currency": "EUR"}, payment["amount"])

	captureReqBody = `{
		"amount": "1"
	}`
	captureResBody, res, err = captureRequest(c, id, captureReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusBadRequest, res.StatusCode)
	require.Equal(t, map[string]interface{}{"error": "payment is not eligible for this action"}, captureResBody)
}

func captureRequest(c http.Client, id, body string) (map[string]interface{}, *http.Response, error) {
	return request(c, http.MethodPut, baseUrl+"/"+id+"/capture", body)
}
