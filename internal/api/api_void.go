package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/wojtii/dc-challenge/internal/payment"
)

type voidRequest struct {
	uri idURIRequest
}

func (h handler) void(c *gin.Context) {
	var request voidRequest

	if err := c.ShouldBindUri(&request.uri); err != nil {
		validationErrorResponse(c)
		return
	}

	in := payment.CancelUseCaseInput{
		ID: request.uri.ID,
	}

	p, err := h.cancelUseCase.Run(c, in)
	if err != nil {
		handlePaymentError(c, err)
		return
	}

	response := newPaymentResponse(p)
	c.JSON(http.StatusOK, response)
}
