package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/shopspring/decimal"
	"gitlab.com/wojtii/dc-challenge/internal/payment"
)

type captureRequest struct {
	uri  idURIRequest
	body captureRequestBody
}

type captureRequestBody struct {
	Amount decimal.Decimal `json:"amount" binding:"required"`
}

func (h handler) capture(c *gin.Context) {
	var request captureRequest

	if err := c.ShouldBindUri(&request.uri); err != nil {
		validationErrorResponse(c)
		return
	}

	if err := c.ShouldBindJSON(&request.body); err != nil {
		validationErrorResponse(c)
		return
	}

	in := payment.CaptureUseCaseInput{
		ID:     request.uri.ID,
		Amount: request.body.Amount,
	}

	p, err := h.captureUseCase.Run(c, in)
	if err != nil {
		handlePaymentError(c, err)
		return
	}

	response := newPaymentResponse(p)
	c.JSON(http.StatusOK, response)
}
