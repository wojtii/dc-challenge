package api

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/shopspring/decimal"
	"gitlab.com/wojtii/dc-challenge/internal/payment"
)

type authorizeRequest struct {
	body authorizeRequestBody
}

type authorizeRequestBody struct {
	CreditCard authorizeRequestBodyCreditCard `json:"creditCard" binding:"required"`
	Amount     authorizeRequestBodyAmount     `json:"amount" binding:"required"`
}

type authorizeRequestBodyCreditCard struct {
	Number    string                                  `json:"number" binding:"required"`
	CVV       string                                  `json:"cvv" binding:"required"`
	ExpiresAt authorizeRequestBodyCreditCardExpiresAt `json:"expiresAt" binding:"required"`
}

type authorizeRequestBodyCreditCardExpiresAt struct {
	Year  int `json:"year" binding:"required,gte=2022,lte=2100"`
	Month int `json:"month" binding:"required,gte=1,lte=12"`
}

type authorizeRequestBodyAmount struct {
	Amount   decimal.Decimal `json:"amount" binding:"required"`
	Currency string          `json:"currency" binding:"required"`
}

func (h handler) authorize(c *gin.Context) {
	var request authorizeRequest

	if err := c.ShouldBindJSON(&request.body); err != nil {
		validationErrorResponse(c)
		return
	}

	in := payment.AuthorizeUseCaseInput{
		Amount: payment.Amount{
			Amount:   request.body.Amount.Amount,
			Currency: request.body.Amount.Currency,
		},
		CreditCard: payment.CreditCard{
			Number: request.body.CreditCard.Number,
			CVV:    request.body.CreditCard.CVV,
			ExpiresAt: time.Date(
				request.body.CreditCard.ExpiresAt.Year,
				time.Month(request.body.CreditCard.ExpiresAt.Month),
				1, 0, 0, 0, 0, time.UTC,
			),
		},
	}

	p, err := h.authorizeUseCase.Run(c, in)
	if err != nil {
		handlePaymentError(c, err)
		return
	}

	response := newPaymentResponse(p)
	c.JSON(http.StatusCreated, response)
}
