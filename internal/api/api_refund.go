package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/shopspring/decimal"
	"gitlab.com/wojtii/dc-challenge/internal/payment"
)

type refundRequest struct {
	uri  idURIRequest
	body refundRequestBody
}

type refundRequestBody struct {
	Amount decimal.Decimal `json:"amount" binding:"required"`
}

func (h handler) refund(c *gin.Context) {
	var request refundRequest

	if err := c.ShouldBindUri(&request.uri); err != nil {
		validationErrorResponse(c)
		return
	}

	if err := c.ShouldBindJSON(&request.body); err != nil {
		validationErrorResponse(c)
		return
	}

	in := payment.RefundUseCaseInput{
		ID:     request.uri.ID,
		Amount: request.body.Amount,
	}

	p, err := h.refundUseCase.Run(c, in)
	if err != nil {
		handlePaymentError(c, err)
		return
	}

	response := newPaymentResponse(p)
	c.JSON(http.StatusOK, response)
}
