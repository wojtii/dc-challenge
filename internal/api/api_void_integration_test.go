package api_test

import (
	"encoding/json"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestIntegration_Void(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping integration test")
	}

	t.Parallel()

	c := http.Client{Timeout: time.Second * 10}
	authorizeReqBody := `{
		"creditCard": {
			"number": "2222410740360010",
			"cvv": "737",
			"expiresAt": {
				"year": 2030,
				"month": 1
			}
		},
		"amount": {
			"amount": "123.45",
			"currency": "USD"
		}
	}`
	authorizeResBody, res, err := authorizeRequest(c, authorizeReqBody)
	require.NoError(t, err)
	require.Equal(t, http.StatusCreated, res.StatusCode)

	payment, ok := authorizeResBody["payment"].(map[string]interface{})
	require.True(t, ok)

	id, ok := payment["id"].(string)
	require.True(t, ok)
	require.NotEmpty(t, id)
	require.Equal(t, map[string]interface{}{"amountAvailable": "123.45", "currency": "USD"}, payment["amount"])

	voidResBody, res, err := voidRequest(c, id)
	require.NoError(t, err)
	require.Equal(t, http.StatusOK, res.StatusCode)

	payment, ok = voidResBody["payment"].(map[string]interface{})
	require.True(t, ok)

	id, ok = payment["id"].(string)
	require.True(t, ok)
	require.NotEmpty(t, id)
	require.Equal(t, map[string]interface{}{"amountAvailable": "0", "currency": "USD"}, payment["amount"])
}

func authorizeRequest(c http.Client, body string) (map[string]interface{}, *http.Response, error) {
	return request(c, http.MethodPost, baseUrl+"/authorize", body)
}

func voidRequest(c http.Client, id string) (map[string]interface{}, *http.Response, error) {
	return request(c, http.MethodPut, baseUrl+"/"+id+"/void", "{}")
}

func request(c http.Client, method, url, body string) (map[string]interface{}, *http.Response, error) {
	req, err := http.NewRequest(method, url, strings.NewReader(body))
	if err != nil {
		return nil, nil, err
	}

	res, err := c.Do(req)
	if err != nil {
		return nil, nil, err
	}

	defer func() { _ = res.Body.Close() }()

	var result map[string]interface{}
	err = json.NewDecoder(res.Body).Decode(&result)
	return result, res, err
}

const baseUrl = "http://localhost:8080/v1/payments"
