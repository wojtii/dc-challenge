package api

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/shopspring/decimal"
	"gitlab.com/wojtii/dc-challenge/internal/payment"
	"go.uber.org/zap"
)

func Init(g gin.IRouter, r payment.Repository) {
	h := handler{
		authorizeUseCase: payment.NewAuthorizeUseCase(r),
		cancelUseCase:    payment.NewCancelUseCase(r),
		captureUseCase:   payment.NewCaptureUseCase(r),
		refundUseCase:    payment.NewRefundUseCase(r),
	}

	v1 := g.Group("/v1/payments")
	v1.POST("/authorize", h.authorize)
	v1.PUT("/:id/capture", h.capture)
	v1.PUT("/:id/void", h.void)
	v1.PUT("/:id/refund", h.refund)
}

type handler struct {
	authorizeUseCase payment.AuthorizeUseCase
	cancelUseCase    payment.CancelUseCase
	captureUseCase   payment.CaptureUseCase
	refundUseCase    payment.RefundUseCase
}

type idURIRequest struct {
	ID string `uri:"id" binding:"required,uuid"`
}

func newPaymentResponse(p payment.Payment) paymentResponse {
	return paymentResponse{
		Payment: paymentResponseItem{
			ID: p.ID,
			Amount: amountResponse{
				AmountAvailable: p.Amount.Amount,
				Currency:        p.Amount.Currency,
			},
		},
	}
}

type paymentResponse struct {
	Payment paymentResponseItem `json:"payment"`
}

type paymentResponseItem struct {
	ID     string         `json:"id"`
	Amount amountResponse `json:"amount"`
}

type amountResponse struct {
	AmountAvailable decimal.Decimal `json:"amountAvailable"`
	Currency        string          `json:"currency"`
}

type errorResponse struct {
	Error string `json:"error"`
}

func validationErrorResponse(c *gin.Context) {
	c.JSON(http.StatusBadRequest, errorResponse{Error: "validation error"})
}

func serverErrorResponse(c *gin.Context) {
	code := http.StatusInternalServerError
	c.JSON(code, errorResponse{Error: http.StatusText(code)})
}

func handlePaymentError(c *gin.Context, err error) {
	var paymentErr payment.Error
	if errors.As(err, &paymentErr) {
		c.JSON(http.StatusBadRequest, errorResponse{Error: paymentErr.Error()})
		return
	}

	zap.S().Errorw("internal server error", zap.Error(err))
	serverErrorResponse(c)
}
