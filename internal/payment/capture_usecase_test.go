package payment_test

import (
	"context"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/wojtii/dc-challenge/internal/payment"
)

func TestCaptureUseCase_Run(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		repo    payment.FakeRepository
		in      payment.CaptureUseCaseInput
		want    payment.Payment
		wantErr *payment.Error
	}{
		"given not positive amount, return error": {
			repo: payment.FakeRepository{},
			in: payment.CaptureUseCaseInput{
				ID:     "00000000-0000-0000-0000-111111111111",
				Amount: decimal.Zero,
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrAmountNotPositive,
		},

		"given empty repository, return error": {
			repo: payment.FakeRepository{},
			in: payment.CaptureUseCaseInput{
				ID:     "00000000-0000-0000-0000-444444444444",
				Amount: decimal.NewFromFloat(0.01),
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrNotFound,
		},

		"given test error credit card, return error": {
			repo: payment.FakeRepository{Payments: []payment.Payment{{
				ID: "00000000-0000-0000-0000-111111111111",
				CreditCard: payment.CreditCard{
					Number: "4000000000000259",
				},
			}}},
			in: payment.CaptureUseCaseInput{
				ID:     "00000000-0000-0000-0000-111111111111",
				Amount: decimal.NewFromFloat(100),
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrCaptureFailure,
		},

		"given payment with status that can not be captured, return error": {
			repo: payment.FakeRepository{Payments: []payment.Payment{{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusCancelled,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(123.45),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			}}},
			in: payment.CaptureUseCaseInput{
				ID:     "00000000-0000-0000-0000-111111111111",
				Amount: decimal.NewFromFloat(100),
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrPaymentNotEligible,
		},

		"given payment with not enough amount to capture, return error": {
			repo: payment.FakeRepository{Payments: []payment.Payment{{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusCaptured,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(23.45),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			}}},
			in: payment.CaptureUseCaseInput{
				ID:     "00000000-0000-0000-0000-111111111111",
				Amount: decimal.NewFromFloat(100),
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrAmountTooBig,
		},

		"given valid payment, when capturing with not full amount, return captured payment": {
			repo: payment.FakeRepository{Payments: []payment.Payment{{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusAuthorized,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(23.45),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			}}},
			in: payment.CaptureUseCaseInput{
				ID:     "00000000-0000-0000-0000-111111111111",
				Amount: decimal.NewFromFloat(10),
			},
			want: payment.Payment{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusCaptured,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(23.45).Sub(decimal.NewFromFloat(10)),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			},
		},

		"given valid payment, when capturing with full amount, return done payment": {
			repo: payment.FakeRepository{Payments: []payment.Payment{{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusCaptured,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(23.45),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			}}},
			in: payment.CaptureUseCaseInput{
				ID:     "00000000-0000-0000-0000-111111111111",
				Amount: decimal.NewFromFloat(23.45),
			},
			want: payment.Payment{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusDone,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(23.45).Sub(decimal.NewFromFloat(23.45)),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			},
		},
	}

	for name := range testCases {
		tc := testCases[name]
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			uc := payment.NewCaptureUseCase(&tc.repo)

			got, err := uc.Run(context.Background(), tc.in)
			if tc.wantErr != nil {
				assert.ErrorIs(t, err, *tc.wantErr)
			} else {
				assert.NoError(t, err)
			}

			assert.Equal(t, tc.want, got)
		})
	}
}
