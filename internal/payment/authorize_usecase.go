package payment

import (
	"context"
	"fmt"
	"time"

	creditcard "github.com/durango/go-credit-card"
)

func NewAuthorizeUseCase(r Repository) AuthorizeUseCase {
	return AuthorizeUseCase{
		repository: r,
	}
}

type AuthorizeUseCase struct {
	repository Repository
}

type AuthorizeUseCaseInput struct {
	Amount     Amount
	CreditCard CreditCard
}

func (uc AuthorizeUseCase) Run(ctx context.Context, in AuthorizeUseCaseInput) (Payment, error) {
	if in.CreditCard.Number == "4000000000000119" {
		return Payment{}, ErrAuthorizationFailure
	}

	if !in.Amount.isSupportedCurrency() {
		return Payment{}, ErrUnsupportedCurrency
	}

	if !in.Amount.Amount.IsPositive() {
		return Payment{}, ErrAmountNotPositive
	}

	tomorrow := time.Now().UTC().AddDate(0, 1, 1)
	if in.CreditCard.ExpiresAt.Before(tomorrow) {
		return Payment{}, ErrCreditCardExpired
	}

	card := creditcard.Card{
		Number: in.CreditCard.Number,
		Cvv:    in.CreditCard.CVV,
		Month:  fmt.Sprintf("%02d", in.CreditCard.ExpiresAt.Month()),
		Year:   fmt.Sprint(in.CreditCard.ExpiresAt.Year()),
	}
	if err := card.Validate(); err != nil {
		return Payment{}, ErrInvalidCard
	}

	return uc.repository.Authorize(ctx, in)
}
