insert into payments (
  id,
  status,
  amount,
  currency,
  credit_card_number,
  credit_card_cvv,
  credit_card_expires_at,
  created_at,
  updated_at
)
values (
  uuid_generate_v4(),
  $1,
  $2,
  $3,
  $4,
  $5,
  $6,
  now(),
  now()
)
returning *;
