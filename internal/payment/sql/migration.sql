begin;

create extension if not exists "uuid-ossp";

create table if not exists payments (
  id uuid primary key,
  status varchar(16) not null,
  amount numeric(15, 6) not null check (amount >= 0),
  currency varchar(16) not null,
  credit_card_number varchar(32) not null,
  credit_card_cvv varchar(8) not null,
  credit_card_expires_at timestamp with time zone not null,
  created_at timestamp with time zone not null,
  updated_at timestamp with time zone not null
);

commit;
