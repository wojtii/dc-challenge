update payments
set
  status = $2,
  amount = 0,
  updated_at = now()
where id = $1
returning *;
