update payments
set
  amount = amount - $2,
  status = $3,
  updated_at = now()
where id = $1
returning *;
