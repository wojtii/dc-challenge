package payment

import (
	"time"

	"github.com/shopspring/decimal"
)

type Payment struct {
	ID         string
	Status     Status
	Amount     Amount
	CreditCard CreditCard
	CreatedAt  time.Time
	UpdatedAt  time.Time
}

type Amount struct {
	Amount   decimal.Decimal
	Currency string
}

func (a Amount) isSupportedCurrency() bool {
	_, ok := supportedCurrencies[a.Currency]
	return ok
}

type CreditCard struct {
	Number    string
	CVV       string
	ExpiresAt time.Time
}

type Status string

func (s Status) canCapture() bool {
	_, ok := canCaptureStatuses[s]
	return ok
}

func (s Status) canCancel() bool {
	_, ok := canCancelStatuses[s]
	return ok
}

func (s Status) canRefund() bool {
	_, ok := canRefundStatuses[s]
	return ok
}

const (
	StatusAuthorized Status = "AUTHORIZED"
	StatusCaptured   Status = "CAPTURED"
	StatusCancelled  Status = "CANCELLED"
	StatusRefunded   Status = "REFUNDED"
	StatusDone       Status = "DONE"
)

var (
	canCaptureStatuses = map[Status]struct{}{
		StatusAuthorized: {},
		StatusCaptured:   {},
	}

	canCancelStatuses = map[Status]struct{}{
		StatusAuthorized: {},
	}

	canRefundStatuses = map[Status]struct{}{
		StatusAuthorized: {},
		StatusCaptured:   {},
		StatusRefunded:   {},
	}
)

var supportedCurrencies = map[string]struct{}{
	"USD": {},
	"EUR": {},
	"PLN": {},
}
