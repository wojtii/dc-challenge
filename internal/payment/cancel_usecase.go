package payment

import (
	"context"
)

func NewCancelUseCase(r Repository) CancelUseCase {
	return CancelUseCase{
		repository: r,
	}
}

type CancelUseCase struct {
	repository Repository
}

type CancelUseCaseInput struct {
	ID string
}

func (uc CancelUseCase) Run(ctx context.Context, in CancelUseCaseInput) (Payment, error) {
	p, err := uc.repository.WithTx(ctx, func(tr TxRepository) (Payment, error) {
		p, err := tr.SelectForUpdate(ctx, in.ID)
		if err != nil {
			return Payment{}, err
		}

		if !p.Status.canCancel() {
			return Payment{}, ErrPaymentNotEligible
		}

		return tr.Cancel(ctx, in)
	})

	return p, err
}
