package payment

import (
	"context"
	"time"
)

type FakeRepository struct {
	Payments []Payment
}

func (FakeRepository) Migrate(context.Context) error {
	return nil
}

func (r *FakeRepository) Authorize(ctx context.Context, in AuthorizeUseCaseInput) (Payment, error) {
	p := Payment{
		ID:         "00000000-0000-0000-0000-111111111111",
		Status:     StatusAuthorized,
		Amount:     in.Amount,
		CreditCard: in.CreditCard,
		CreatedAt:  time.Time{},
		UpdatedAt:  time.Time{},
	}
	r.Payments = append(r.Payments, p)
	return p, nil
}

func (r FakeRepository) WithTx(ctx context.Context, f func(TxRepository) (Payment, error)) (Payment, error) {
	return f(&FakeTxRepository{r.Payments})
}

type FakeTxRepository struct {
	payments []Payment
}

func (r FakeTxRepository) SelectForUpdate(ctx context.Context, id string) (Payment, error) {
	for _, p := range r.payments {
		if p.ID == id {
			return p, nil
		}
	}

	return Payment{}, ErrNotFound
}

func (r *FakeTxRepository) Capture(ctx context.Context, in CaptureRepositoryInput) (Payment, error) {
	for idx, p := range r.payments {
		if p.ID == in.ID {
			newP := Payment{
				ID:     p.ID,
				Status: in.Status,
				Amount: Amount{
					Amount:   p.Amount.Amount.Sub(in.Amount),
					Currency: p.Amount.Currency,
				},
				CreditCard: p.CreditCard,
				CreatedAt:  p.CreatedAt,
				UpdatedAt:  p.UpdatedAt,
			}
			r.payments[idx] = newP
			return newP, nil
		}
	}
	return Payment{}, ErrNotFound
}

func (r *FakeTxRepository) Cancel(ctx context.Context, in CancelUseCaseInput) (Payment, error) {
	for idx, p := range r.payments {
		if p.ID == in.ID {
			newP := Payment{
				ID:         p.ID,
				Status:     StatusCancelled,
				Amount:     p.Amount,
				CreditCard: p.CreditCard,
				CreatedAt:  p.CreatedAt,
				UpdatedAt:  p.UpdatedAt,
			}
			r.payments[idx] = newP
			return newP, nil
		}
	}
	return Payment{}, ErrNotFound
}

func (r *FakeTxRepository) Refund(ctx context.Context, in RefundRepositoryInput) (Payment, error) {
	for idx, p := range r.payments {
		if p.ID == in.ID {
			newP := Payment{
				ID:     p.ID,
				Status: in.Status,
				Amount: Amount{
					Amount:   p.Amount.Amount.Sub(in.Amount),
					Currency: p.Amount.Currency,
				},
				CreditCard: p.CreditCard,
				CreatedAt:  p.CreatedAt,
				UpdatedAt:  p.UpdatedAt,
			}
			r.payments[idx] = newP
			return newP, nil
		}
	}
	return Payment{}, ErrNotFound
}
