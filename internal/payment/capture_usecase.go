package payment

import (
	"context"

	"github.com/shopspring/decimal"
)

func NewCaptureUseCase(r Repository) CaptureUseCase {
	return CaptureUseCase{
		repository: r,
	}
}

type CaptureUseCase struct {
	repository Repository
}

type CaptureUseCaseInput struct {
	ID     string
	Amount decimal.Decimal
}

func (uc CaptureUseCase) Run(ctx context.Context, in CaptureUseCaseInput) (Payment, error) {
	if !in.Amount.IsPositive() {
		return Payment{}, ErrAmountNotPositive
	}

	p, err := uc.repository.WithTx(ctx, func(tr TxRepository) (Payment, error) {
		p, err := tr.SelectForUpdate(ctx, in.ID)
		if err != nil {
			return Payment{}, err
		}

		if p.CreditCard.Number == "4000000000000259" {
			return Payment{}, ErrCaptureFailure
		}

		if !p.Status.canCapture() {
			return Payment{}, ErrPaymentNotEligible
		}

		if in.Amount.GreaterThan(p.Amount.Amount) {
			return Payment{}, ErrAmountTooBig
		}

		status := StatusCaptured
		if p.Amount.Amount.Sub(in.Amount).IsZero() {
			status = StatusDone
		}

		return tr.Capture(ctx, CaptureRepositoryInput{
			CaptureUseCaseInput: in,
			Status:              status,
		})
	})

	return p, err
}
