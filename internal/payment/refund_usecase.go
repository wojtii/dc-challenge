package payment

import (
	"context"

	"github.com/shopspring/decimal"
)

func NewRefundUseCase(r Repository) RefundUseCase {
	return RefundUseCase{
		repository: r,
	}
}

type RefundUseCase struct {
	repository Repository
}

type RefundUseCaseInput struct {
	ID     string
	Amount decimal.Decimal
}

func (uc RefundUseCase) Run(ctx context.Context, in RefundUseCaseInput) (Payment, error) {
	if !in.Amount.IsPositive() {
		return Payment{}, ErrAmountNotPositive
	}

	p, err := uc.repository.WithTx(ctx, func(tr TxRepository) (Payment, error) {
		p, err := tr.SelectForUpdate(ctx, in.ID)
		if err != nil {
			return Payment{}, err
		}

		if p.CreditCard.Number == "4000000000003238" {
			return Payment{}, ErrRefundFailure
		}

		if !p.Status.canRefund() {
			return Payment{}, ErrPaymentNotEligible
		}

		if in.Amount.GreaterThan(p.Amount.Amount) {
			return Payment{}, ErrAmountTooBig
		}

		status := StatusRefunded
		if p.Amount.Amount.Sub(in.Amount).IsZero() {
			status = StatusDone
		}

		return tr.Refund(ctx, RefundRepositoryInput{
			RefundUseCaseInput: in,
			Status:             status,
		})
	})

	return p, err
}
