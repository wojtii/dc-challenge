package payment

type Error struct {
	msg string
}

func (e Error) Error() string {
	return e.msg
}

var (
	ErrNotFound            = Error{"payment not found"}
	ErrAmountNotPositive   = Error{"amount must be positive"}
	ErrPaymentNotEligible  = Error{"payment is not eligible for this action"}
	ErrAmountTooBig        = Error{"amount is too big"}
	ErrUnsupportedCurrency = Error{"unsupported currency"}
	ErrInvalidCard         = Error{"invalid card"}
	ErrCreditCardExpired   = Error{"credit card is expired or will expire soon"}

	ErrAuthorizationFailure = Error{"authorization failure"}
	ErrCaptureFailure       = Error{"capture failure"}
	ErrRefundFailure        = Error{"refund failure"}
)
