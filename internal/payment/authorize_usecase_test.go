package payment_test

import (
	"context"
	"testing"
	"time"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/wojtii/dc-challenge/internal/payment"
)

func TestAuthorizeUseCase_Run(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		in      payment.AuthorizeUseCaseInput
		want    payment.Payment
		wantErr *payment.Error
	}{
		"given test error credit card, return error": {
			in: payment.AuthorizeUseCaseInput{
				CreditCard: payment.CreditCard{
					Number: "4000000000000119",
				},
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrAuthorizationFailure,
		},

		"given unsupported currency, return error": {
			in: payment.AuthorizeUseCaseInput{
				Amount: payment.Amount{
					Currency: "UNSUPPORTED",
				},
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrUnsupportedCurrency,
		},

		"given not positive amount, return error": {
			in: payment.AuthorizeUseCaseInput{
				Amount: payment.Amount{
					Currency: "EUR",
					Amount:   decimal.NewFromFloat(-10.987),
				},
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrAmountNotPositive,
		},

		"given expiring credit card, return error": {
			in: payment.AuthorizeUseCaseInput{
				Amount: payment.Amount{
					Currency: "EUR",
					Amount:   decimal.NewFromFloat(1),
				},
				CreditCard: payment.CreditCard{
					ExpiresAt: time.Now().UTC(),
				},
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrCreditCardExpired,
		},

		"given invalid card, return error": {
			in: payment.AuthorizeUseCaseInput{
				CreditCard: payment.CreditCard{
					Number:    "invalid",
					CVV:       "$$$",
					ExpiresAt: time.Now().UTC().AddDate(1, 0, 0),
				},
				Amount: payment.Amount{
					Currency: "USD",
					Amount:   decimal.NewFromFloat(1.23),
				},
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrInvalidCard,
		},

		"given valid card, return new payment": {
			in: payment.AuthorizeUseCaseInput{
				CreditCard: testCreditCard,
				Amount: payment.Amount{
					Currency: "USD",
					Amount:   decimal.NewFromFloat(10),
				},
			},
			want: payment.Payment{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusAuthorized,
				Amount: payment.Amount{
					Currency: "USD",
					Amount:   decimal.NewFromFloat(10),
				},
				CreditCard: testCreditCard,
			},
		},
	}

	for name := range testCases {
		tc := testCases[name]
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			repo := &payment.FakeRepository{}
			uc := payment.NewAuthorizeUseCase(repo)

			got, err := uc.Run(context.Background(), tc.in)
			if tc.wantErr != nil {
				assert.ErrorIs(t, err, *tc.wantErr)
			} else {
				assert.NoError(t, err)
			}

			assert.Equal(t, tc.want, got)
		})
	}
}

var testCreditCard = payment.CreditCard{
	Number:    "2222410740360010",
	CVV:       "737",
	ExpiresAt: time.Date(2030, time.March, 1, 0, 0, 0, 0, time.UTC),
}
