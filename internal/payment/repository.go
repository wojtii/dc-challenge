package payment

import (
	"context"
	_ "embed"
	"errors"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Repository interface {
	Migrate(ctx context.Context) error
	Authorize(ctx context.Context, in AuthorizeUseCaseInput) (Payment, error)
	WithTx(ctx context.Context, f func(TxRepository) (Payment, error)) (Payment, error)
}

func NewPostgresRepository(db *pgxpool.Pool) PostgresRepository {
	return PostgresRepository{db}
}

type PostgresRepository struct {
	db *pgxpool.Pool
}

func (r PostgresRepository) Migrate(ctx context.Context) error {
	_, err := r.db.Exec(ctx, migrationSQL)
	return err
}

func (r PostgresRepository) Authorize(ctx context.Context, in AuthorizeUseCaseInput) (Payment, error) {
	row := r.db.QueryRow(
		ctx,
		authorizeSQL,
		StatusAuthorized,
		in.Amount.Amount,
		in.Amount.Currency,
		in.CreditCard.Number,
		in.CreditCard.CVV,
		in.CreditCard.ExpiresAt,
	)
	return scanToPayment(row)
}

func (r PostgresRepository) WithTx(ctx context.Context, f func(TxRepository) (Payment, error)) (Payment, error) {
	txOpts := pgx.TxOptions{IsoLevel: pgx.Serializable}
	tx, err := r.db.BeginTx(ctx, txOpts)
	if err != nil {
		return Payment{}, err
	}

	defer func() {
		_ = tx.Rollback(ctx)
	}()

	p, err := f(PostgresTxRepository{tx})
	if err != nil {
		return Payment{}, err
	}

	err = tx.Commit(ctx)
	if err != nil {
		return Payment{}, err
	}

	return p, nil
}

type TxRepository interface {
	SelectForUpdate(ctx context.Context, id string) (Payment, error)
	Capture(ctx context.Context, in CaptureRepositoryInput) (Payment, error)
	Cancel(ctx context.Context, in CancelUseCaseInput) (Payment, error)
	Refund(ctx context.Context, in RefundRepositoryInput) (Payment, error)
}

type PostgresTxRepository struct {
	tx pgx.Tx
}

func (r PostgresTxRepository) SelectForUpdate(ctx context.Context, id string) (Payment, error) {
	row := r.tx.QueryRow(ctx, selectForUpdateSQL, id)

	p, err := scanToPayment(row)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			err = ErrNotFound
		}

		return Payment{}, err
	}

	return p, err
}

type CaptureRepositoryInput struct {
	CaptureUseCaseInput
	Status Status
}

func (r PostgresTxRepository) Capture(ctx context.Context, in CaptureRepositoryInput) (Payment, error) {
	row := r.tx.QueryRow(ctx, captureSQL, in.ID, in.Amount, in.Status)
	p, err := scanToPayment(row)
	return p, err
}

func (r PostgresTxRepository) Cancel(ctx context.Context, in CancelUseCaseInput) (Payment, error) {
	row := r.tx.QueryRow(ctx, cancelSQL, in.ID, StatusCancelled)
	return scanToPayment(row)
}

type RefundRepositoryInput struct {
	RefundUseCaseInput
	Status Status
}

func (r PostgresTxRepository) Refund(ctx context.Context, in RefundRepositoryInput) (Payment, error) {
	row := r.tx.QueryRow(ctx, refundSQL, in.ID, in.Amount, in.Status)
	return scanToPayment(row)
}

func scanToPayment(row pgx.Row) (Payment, error) {
	var p Payment
	err := row.Scan(
		&p.ID,
		&p.Status,
		&p.Amount.Amount,
		&p.Amount.Currency,
		&p.CreditCard.Number,
		&p.CreditCard.CVV,
		&p.CreditCard.ExpiresAt,
		&p.CreatedAt,
		&p.UpdatedAt,
	)

	return p, err
}

var (
	//go:embed sql/migration.sql
	migrationSQL string
	//go:embed sql/authorize.sql
	authorizeSQL string
	//go:embed sql/select_for_update.sql
	selectForUpdateSQL string
	//go:embed sql/cancel.sql
	cancelSQL string
	//go:embed sql/capture.sql
	captureSQL string
	//go:embed sql/refund.sql
	refundSQL string
)
