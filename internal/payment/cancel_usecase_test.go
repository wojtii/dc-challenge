package payment_test

import (
	"context"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/wojtii/dc-challenge/internal/payment"
)

func TestCancelUseCase_Run(t *testing.T) {
	t.Parallel()

	testCases := map[string]struct {
		repo    payment.FakeRepository
		in      payment.CancelUseCaseInput
		want    payment.Payment
		wantErr *payment.Error
	}{
		"given payment that does not exist, return error": {
			repo: payment.FakeRepository{Payments: []payment.Payment{{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusCaptured,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(1.23),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			}}},
			in: payment.CancelUseCaseInput{
				ID: "00000000-0000-0000-0000-444444444444",
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrNotFound,
		},

		"given payment with status that can not be closed, return error": {
			repo: payment.FakeRepository{Payments: []payment.Payment{{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusCaptured,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(1.23),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			}}},
			in: payment.CancelUseCaseInput{
				ID: "00000000-0000-0000-0000-111111111111",
			},
			want:    payment.Payment{},
			wantErr: &payment.ErrPaymentNotEligible,
		},

		"given payment with status that can be closed, cancel and return payment": {
			repo: payment.FakeRepository{Payments: []payment.Payment{{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusAuthorized,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(1.23),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			}}},
			in: payment.CancelUseCaseInput{
				ID: "00000000-0000-0000-0000-111111111111",
			},
			want: payment.Payment{
				ID:     "00000000-0000-0000-0000-111111111111",
				Status: payment.StatusCancelled,
				Amount: payment.Amount{
					Amount:   decimal.NewFromFloat(1.23),
					Currency: "USD",
				},
				CreditCard: testCreditCard,
			},
		},
	}

	for name := range testCases {
		tc := testCases[name]
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			uc := payment.NewCancelUseCase(&tc.repo)

			got, err := uc.Run(context.Background(), tc.in)
			if tc.wantErr != nil {
				assert.ErrorIs(t, err, *tc.wantErr)
			} else {
				assert.NoError(t, err)
			}

			assert.Equal(t, tc.want, got)
		})
	}
}
