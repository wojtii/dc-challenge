package postgres

import (
	"context"
	"fmt"
	"os"

	"github.com/jackc/pgx/v4/pgxpool"
)

func Setup(ctx context.Context) (*pgxpool.Pool, error) {
	const (
		userEnv     = "POSTGRES_USER"
		dbEnv       = "POSTGRES_DB"
		passwordEnv = "POSTGRES_PASSWORD"
		hostEnv     = "POSTGRES_HOST"
	)

	user, ok := os.LookupEnv(userEnv)
	if !ok {
		return nil, fmt.Errorf("missing %q env", userEnv)
	}

	dbName, ok := os.LookupEnv(dbEnv)
	if !ok {
		return nil, fmt.Errorf("missing %q env", dbEnv)
	}

	password, ok := os.LookupEnv(passwordEnv)
	if !ok {
		return nil, fmt.Errorf("missing %q env", passwordEnv)
	}

	host, ok := os.LookupEnv(hostEnv)
	if !ok {
		return nil, fmt.Errorf("missing %q env", hostEnv)
	}

	connString := fmt.Sprintf("postgresql://%s:%s@%s:5432/%s", user, password, host, dbName)
	db, err := pgxpool.Connect(ctx, connString)
	if err != nil {
		return nil, err
	}

	err = db.Ping(ctx)
	if err != nil {
		return nil, err
	}

	return db, nil
}
