FROM golang AS build
WORKDIR /app
COPY . ./
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build

FROM alpine
RUN apk add --no-cache ca-certificates && update-ca-certificates
WORKDIR /app
COPY --from=build /app/dc-challenge /app/dc-challenge
EXPOSE 8080 8080
ENTRYPOINT ["/app/dc-challenge"]
